import {
  GraphQLEnumType,
  GraphQLString,
  GraphQLList,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLID
} from 'graphql'
import {
  GraphQLEmailType
} from 'gql/types'
import Dog from 'database/models/vertices/Dog'
import DogToOwner from 'database/models/edges/DogToOwner'
import DogType from 'gql/models/Dog/DogType'

const User = new GraphQLObjectType({
  name: 'User',
  description: 'User type definition',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID),
      description: `The user's ID`,
      resolve: user => user._id
    },
    username: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'Unique username'
    },
    userType: {
      type: new GraphQLEnumType({
        name: 'userType',
        values: {
          'runner': { value: 0 },
          'owner': { value: 1 }
        }
      }),
      description: 'Runner or Owner'
    },
    email: {
      type: new GraphQLNonNull(GraphQLEmailType),
      description: `The user's email`
    },
    token: {
      type: GraphQLString,
      description: 'JWT token for authorization'
    },
    dogs: {
      type: new GraphQLList(DogType),
      description: `The dog's owner`,
      resolve: async dog => {
        try {
          const ownerEdge = await DogToOwner.byExample({ _to: dog._id })
          const ownerEdgeData = await ownerEdge.next()

          const user = await Dog.document(ownerEdgeData._from)
          const userData = await user.next()

          return userData
        } catch (error) {
          throw new Error(error)
        }
      }
    }
  })
})

export default User
