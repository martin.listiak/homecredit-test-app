import { GraphQLScalarType } from 'graphql'
import { GraphQLError } from 'graphql/error'
import { Kind } from 'graphql/language'
import { emailRegex } from 'utils/regex'

const GraphQLEmailType = new GraphQLScalarType({
  name: 'Email',
  serialize: value => value.toLowerCase(),
  parseValue: value => value.toLowerCase(),
  parseLiteral: valueAST => {
    if (valueAST.kind !== Kind.STRING) {
      throw new GraphQLError(
        `Query error: Email is not a string, it is a: ${valueAST.kind}`,
        [valueAST]
      )
    }

    if (!emailRegex.test(valueAST.value)) {
      throw new GraphQLError('Query error: Not a valid Email', [valueAST])
    }

    if (valueAST.value.length < 4) {
      throw new GraphQLError(
        `Query error: Email must have a minimum length of 4.`,
        [valueAST]
      )
    }

    if (valueAST.value.length > 300) {
      throw new GraphQLError(`Query error: Email is too long.`, [valueAST])
    }

    return valueAST.value.toLowerCase()
  }
})

export default GraphQLEmailType
