import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLID
} from 'graphql'
import { mutationWithClientMutationId } from 'graphql-relay'
import {
  Dog,
  RunnerToDog
} from 'database/models'
import DogType from '../DogType'
import { aclUser } from 'validations/userAuthenticated'
import {
  isNilOrEmpty,
  omitBy
} from 'ramda-adjunct'

const updateDogMutation = mutationWithClientMutationId({
  name: 'UpdateDogMutation',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    name: {
      type: GraphQLString
    },
    age: {
      type: GraphQLInt
    },
    breed: {
      type: GraphQLString
    },
    availableFrom: {
      type: GraphQLString
    },
    availableTo: {
      type: GraphQLString
    },
    appointmentTime: {
      type: GraphQLString
    },
    runner: {
      type: GraphQLID
    }
  },
  outputFields: {
    Dog: {
      type: DogType,
      resolve: ({ Dog }) => Dog
    }
  },
  mutateAndGetPayload: async ({
    id,
    name,
    age,
    breed,
    availableFrom,
    availableTo,
    appointment
  }, { req: { user } }) => {
    try {
      aclUser(user)

      const fieldsToUpdate = omitBy(isNilOrEmpty, {
        name,
        age,
        breed,
        availableFrom,
        availableTo
      })

      if (appointment) {
        await RunnerToDog.save({
          _from: user.id,
          _to: id,
          time: appointment.time
        }, { overwrite: true })
      }

      const newDog = await Dog.updateByExample({ id }, fieldsToUpdate, { returnNew: true })

      return {
        Dog: { ...newDog.new, appointment }
      }
    } catch (err) {
      throw new Error(err)
    }
  }
})

export default updateDogMutation
