import User from './vertices/User'
import Dog from './vertices/Dog'
import DogToOwner from './edges/DogToOwner'
import RunnerToDog from './edges/RunnerToDog'

export {
  User,
  Dog,
  DogToOwner,
  RunnerToDog
}
