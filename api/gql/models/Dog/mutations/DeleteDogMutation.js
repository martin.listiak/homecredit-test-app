import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLID
} from 'graphql'
import { mutationWithClientMutationId } from 'graphql-relay'
import {
  Dog
} from 'database/models'
import { aclUser } from 'validations/userAuthenticated'

const deleteDogMutation = mutationWithClientMutationId({
  name: 'DeleteDogMutation',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    message: {
      type: GraphQLString,
      resolve: ({ message }) => message
    }
  },
  mutateAndGetPayload: async ({
    id
  }, { req: { user } }) => {
    try {
      aclUser(user)

      await Dog.removeByExample({
        id
      })

      return {
        message: 'Dog deleted successfully'
      }
    } catch (err) {
      throw new Error(err)
    }
  }
})

export default deleteDogMutation
