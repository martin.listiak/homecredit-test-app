import express from 'express'
import graphqlHTTP from 'express-graphql'
import cors from 'cors'
import bodyParser from 'body-parser'
import passport from 'passport'
import helmet from 'helmet'
import schema from './gql'
import { authMiddleware } from 'services/auth'

const env = 'dev'
const port = 8080

const startServer = async () => {
  const app = express()

  app.use(cors())
  app.use(helmet())
  app.use(bodyParser.json())
  app.use(bodyParser.text({ type: 'application/graphql' }))

  app.use(passport.initialize())
  app.use(passport.session())

  // assign user data to req.user (if logged in)
  app.use(authMiddleware)

  app.use(
    '/graphql',
    graphqlHTTP((req, res) => ({
      schema,
      graphiql: env === 'dev',
      context: {
        req,
        res
      }
    }))
  )

  return app.listen(port, () => {
    if (env !== 'test') {
      console.log('\n\n\n')
      console.log(`--------- server is ready now ---------`)
      console.log(`Current env: ${env}`)
      console.log(`ArangoDB instance: TODO!!!!`)
      console.log(`Server URL: http://localhost:${port}/graphql`)
      console.log(`---------------------------------------`)
      console.log('\n\n\n')
    }
  })
}

const stopServer = async (app) => {
  app.close()
  if (env !== 'test') {
    console.log('----------- server stopped ------------')
  }
}

export {
  startServer,
  stopServer
}
