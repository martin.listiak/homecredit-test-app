FROM node:latest

RUN mkdir /frontend

WORKDIR /frontend

COPY . ./

RUN npm install

CMD ["npm", "run", "dev"]

EXPOSE 8081
