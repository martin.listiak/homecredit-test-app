import { GraphQLObjectType } from 'graphql'
import userMutation from './models/User/UserMutation'
import dogMutation from './models/Dog/DogMutation'

const rootFields = Object.assign(
  userMutation,
  dogMutation
)

const rootMutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => rootFields
})

export default rootMutation
