import db from 'database/connection'

const mainClearScript = async () => {
  await db.truncate()
}

mainClearScript()
