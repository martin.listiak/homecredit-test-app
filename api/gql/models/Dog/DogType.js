import {
  GraphQLString,
  GraphQLObjectType,
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLID,
  GraphQLInt
} from 'graphql'
import GraphQLAppointmentType from './types/GraphQLAppointmentType.js'
import UserType from 'gql/models/User/UserType'
import {
  User,
  DogToOwner
} from 'database/models'

const Dog = new GraphQLObjectType({
  name: 'Dog',
  description: 'Dog type definition',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID),
      description: `The dog's ID`,
      resolve: dog => dog._id
    },
    name: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'Name of the dog'
    },
    age: {
      type: new GraphQLNonNull(GraphQLInt),
      description: 'Age of the dog'
    },
    breed: {
      type: new GraphQLEnumType({
        name: 'breed',
        values: {
          'chihuahua': { value: 0 },
          'dalmatian': { value: 1 },
          'poodle': { value: 2 },
          'rottweiler': { value: 3 },
          'husky': { value: 4 },
          'retriever': { value: 5 }
        }
      }),
      description: 'Breed of the dog'
    },
    availableFrom: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The time from which is dog available everyday'
    },
    availableTo: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The time which is dog available to everyday'
    },
    owner: {
      type: UserType,
      description: `The dog's owner`,
      resolve: async dog => {
        try {
          const ownerEdge = await DogToOwner.byExample({ _to: dog._id })
          const ownerEdgeData = await ownerEdge.next()

          const user = await User.document(ownerEdgeData._from)
          const userData = await user.next()

          return userData
        } catch (error) {
          throw new Error(error)
        }
      }
    },
    appointment: {
      type: GraphQLAppointmentType,
      description: `The dog's owner`
    }
  })
})

export default Dog
