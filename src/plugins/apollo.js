import apolloClient from 'src/clients/apollo'
import VueApollo from 'vue-apollo'

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})

export default ({ Vue, app }) => {
  Vue.use(VueApollo)
  app.provide = apolloProvider
}
