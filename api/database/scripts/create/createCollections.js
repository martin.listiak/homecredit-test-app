import db from 'database/connection'

const createCollections = async function () {
  db.useDatabase('main')

  const collectionNamesList = [
    'User',
    'Dog'
  ]

  const edgeCollectionNamesList = [
    'RunnerToDog',
    'DogToOwner'
  ]

  try {
    collectionNamesList.forEach(async (collectionName) => {
      const collection = db.collection(collectionName)
      const collectionExists = await collection.exists()

      if (!collectionExists) {
        collection.create(collectionName)
        console.log(`collection "${collectionName}" was created`)
      }
    })

    edgeCollectionNamesList.forEach(async (edgeCollectionName) => {
      const edgeCollection = db.edgeCollection(edgeCollectionName)
      const edgeCollectionExists = await edgeCollection.exists()

      if (!edgeCollectionExists) {
        edgeCollection.create(edgeCollectionName)
        console.log(`edge collection "${edgeCollectionName}" was created`)
      }
    })
  } catch (err) {
    console.log(err)
  }
}

export default createCollections
