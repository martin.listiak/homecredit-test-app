import { Database } from 'arangojs'

const db = new Database(process.env.DATABASE_URL)

export default db
