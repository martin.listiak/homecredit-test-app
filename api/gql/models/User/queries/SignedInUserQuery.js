import User from '../UserType'
import { aclUser } from 'validations/userAuthenticated'

export default {
  type: User,
  description: 'Logged in user',
  resolve: (parentValues, args, { req: { user } }) => {
    aclUser(user)

    return user
  }
}
