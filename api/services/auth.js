import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { Strategy as BearerStrategy } from 'passport-http-bearer'
import argon2 from 'argon2'
import jwt from 'jsonwebtoken'
import { User } from 'database/models'
import {
  INVALID_CREDENTIALS,
  UNAUTHENTICATED
} from 'constants/errorMessages'

const JWTSecret = process.env.JWT_SECRET

passport.serializeUser((user, done) => {
  done(null, user._id)
})

passport.deserializeUser(async (id, done) => {
  try {
    const user = await User.byExample(id)

    done(null, user)
  } catch (err) {
    done(err)
  }
})

passport.use(
  new LocalStrategy({ usernameField: 'username' }, async (username, password, done) => {
    if (!username || !password) {
      return done(null, false, INVALID_CREDENTIALS)
    }
    try {
      const user = await User.byExample({ username: username.toLowerCase() })
      const userData = await user.next()

      if (!userData) {
        return done(null, false, INVALID_CREDENTIALS)
      }

      if (await argon2.verify(userData.password, password)) {
        return done(null, userData)
      } else {
        return done(null, false, INVALID_CREDENTIALS)
      }
    } catch (err) {
      return done(null, false, err.message)
    }
  })
)

passport.use(new BearerStrategy((token, cb) => {
  try {
    jwt.verify(token, JWTSecret, async (tokenError, decoded) => {
      if (tokenError) {
        return cb(null, null)
      }

      if (decoded && decoded.id) {
        const user = await User.document({ _id: decoded.id })

        if (!user) {
          return cb(null, null)
        }
        user.token = token

        return cb(null, user)
      } else {
        cb(null, false)
      }
    })
  } catch (err) {
    throw new Error(err)
  }
}))

const authMiddleware = (req, res, next) => {
  return new Promise((resolve, reject) => {
    passport.authenticate('bearer', (err, user) => {
      if (err) {
        console.log(err)
        reject(new Error(err))
      }

      if (!user) {
        reject(new Error(UNAUTHENTICATED))
      } else {
        req.login(user, err => {
          if (err) {
            reject(new Error(err))
          }

          const payload = {
            id: user._id,
            username: user.username
          }

          user.token = jwt.sign(payload, JWTSecret)
          resolve(user)
        })
      }
    })(req)
  })
    .finally(() => {
      next()
    })
}
const signUp = async ({ email, username, password, req }) => {
  if (!username || !password) {
    throw new Error('You must provide an username and password.')
  }

  try {
    const existingUser = await User.byExample({ username: username.toLowerCase() })
    const existingUserData = await existingUser.next()

    if (existingUserData) {
      throw new Error('username in use')
    }

    const hashedPassword = await argon2.hash(password)

    const user = await User.save({
      email,
      username: username.toLowerCase(),
      password: hashedPassword
    }, { returnNew: true })

    return new Promise((resolve, reject) => {
      req.login(user.new, err => {
        if (err) {
          console.log(err)
          reject(new Error(err))
        }

        const payload = {
          id: user.new._id,
          username
        }
        user.new.token = jwt.sign(payload, JWTSecret)

        resolve(user.new)
      })
    })
  } catch (err) {
    console.log(err)
    throw new Error(err)
  }
}

const signIn = async ({ username, password, req }) => {
  if (!username || !password) {
    throw new Error('You must provide an username and password.')
  }

  return new Promise((resolve, reject) => {
    passport.authenticate('local', (err, user) => {
      if (err) {
        reject(new Error(err))
      }

      if (!user) {
        reject(new Error(INVALID_CREDENTIALS))
      } else {
        req.login(user, err => {
          if (err) {
            reject(new Error(err))
          }

          const payload = {
            id: user._id,
            username
          }

          user.token = jwt.sign(payload, JWTSecret)
          resolve(user)
        })
      }
    })({ body: { username, password } })
  })
}

module.exports = {
  signUp,
  signIn,
  authMiddleware
}
