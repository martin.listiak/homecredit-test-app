import GraphQLEmailType from './scalarTypes/GraphQLEmailType'
import GraphQLPasswordType from './scalarTypes/GraphQLPasswordType'

export {
  GraphQLEmailType,
  GraphQLPasswordType
}
