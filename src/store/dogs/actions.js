import createDogMutation from 'src/gql/Dog/mutations/createDog.gql'
import updateDogMutation from 'src/gql/Dog/mutations/updateDog.gql'
import deleteDogMutation from 'src/gql/Dog/mutations/deleteDog.gql'
import getAllDogsQuery from 'src/gql/Dog/queries/getAllDogs.gql'
// import signedInUser from 'src/gql/User/queries/signedInUser.gql'
import apollo from 'src/clients/apollo'

export async function getAllDogs ({ commit }) {
  try {
    const { data: { getAllDogs: { dogs } } } = await apollo.query({
      query: getAllDogsQuery
    })

    return dogs
  } catch (error) {
    if (error.response && error.response.status === 401) {
      throw new Error('Bad credentials')
    }
    throw new Error(error)
  }
}

export async function createDog ({ commit }, {
  name,
  age,
  availableFrom,
  availableTo,
  breed
}) {
  try {
    const { data: { createDog: { dog } } } = await apollo.mutate({
      mutation: createDogMutation,
      variables: {
        name,
        age,
        availableFrom,
        availableTo,
        breed
      }
    })

    return dog
  } catch (error) {
    if (error.response && error.response.status === 401) {
      throw new Error('Bad credentials')
    }
    throw new Error(error)
  }
}

export async function updateDog ({ commit }, {
  id,
  name,
  ago,
  availableFrom,
  availableTo
}) {
  try {
    const { data: { updateDog: { dog } } } = await apollo.mutate({
      mutation: updateDogMutation,
      variables: {
        id,
        name,
        ago,
        availableFrom,
        availableTo
      }
    })

    return dog
  } catch (error) {
    if (error.response && error.response.status === 401) {
      throw new Error('Bad credentials')
    }
    throw new Error(error)
  }
}

export async function deleteDog ({ commit }, {
  id,
  name,
  ago,
  availableFrom,
  availableTo
}) {
  try {
    const { data: { deleteDog: { dog } } } = await apollo.mutate({
      mutation: deleteDogMutation,
      variables: {
        id
      }
    })

    return dog
  } catch (error) {
    if (error.response && error.response.status === 401) {
      throw new Error('Bad credentials')
    }
    throw new Error(error)
  }
}
