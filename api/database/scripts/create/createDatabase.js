import db from 'database/connection'

const createDatabase = async function () {
  const databaseName = 'main'
  const dbList = await db.listDatabases()

  try {
    if (!dbList.includes(databaseName)) {
      await db.createDatabase(databaseName)
      console.log(`database "${databaseName}" was created`)
    }
  } catch (err) {
    console.log(err)
  }
}

export default createDatabase
