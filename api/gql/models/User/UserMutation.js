import SignInMutation from './mutations/SignInMutation'
import SignUpMutation from './mutations/SignUpMutation'

export default {
  signIn: SignInMutation,
  signUp: SignUpMutation
}
