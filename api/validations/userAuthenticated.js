export const aclUser = user => {
  if (!user || !user._id) {
    throw new Error({
      status: 401,
      message: 'Unauthorized'
    })
  }
}
