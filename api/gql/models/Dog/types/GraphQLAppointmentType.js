import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull
} from 'graphql'
import UserType from 'gql/models/User/UserType'
import {
  User,
  RunnerToDog
} from 'database/models'

const AppointmentType = new GraphQLObjectType({
  name: 'Appointment',
  description: 'Appointment type definition',
  fields: () => ({
    time: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'Time of the pick up'
    },
    runner: {
      type: new GraphQLNonNull(UserType),
      descripion: 'Runner appointed to this very dog',
      resolve: async dog => {
        try {
          const runnerEdge = await RunnerToDog.byExample({ _to: dog._id })
          const runnerEdgeData = await runnerEdge.next()

          const user = await User.document(runnerEdgeData._from)
          const userData = await user.next()

          return userData
        } catch (error) {
          throw new Error(error)
        }
      }
    }
  })
})

export default AppointmentType
