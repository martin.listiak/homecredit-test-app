import signUpMutation from 'src/gql/User/mutations/signUp.gql'
import signInMutation from 'src/gql/User/mutations/signIn.gql'
// import signedInUser from 'src/gql/User/queries/signedInUser.gql'
import apollo from 'src/clients/apollo'
import { Cookies } from 'quasar'

export async function signUp ({ commit }, {
  email,
  password,
  username,
  userType
}) {
  try {
    const { data: { signUp: { token, user } } } = await apollo.mutate({
      mutation: signUpMutation,
      variables: {
        username,
        email,
        password,
        userType
      }
    })

    Cookies.set('user_jwt', token)

    commit('SET_USER', user)

    return signUp
  } catch (err) {
    if (err.response && err.response.status === 401) {
      throw new Error('Bad credentials')
    }
    console.error(err)
    throw new Error(err)
  }
}

// export async function getMe ({ commit }) {
//   try {
//     const { data: { SignedInUser } } = await apollo.query({
//       query: signedInUser
//     })

//     commit('SET_USER', SignedInUser)

//     return SignedInUser
//   } catch (error) {
//     if (error.response && error.response.status === 401) {
//       throw new Error('Bad credentials')
//     }
//     throw error
//   }
// }

export async function signIn ({ commit }, {
  username,
  password
}) {
  try {
    const { data: { signIn: { token, user } } } = await apollo.mutate({
      mutation: signInMutation,
      variables: {
        username,
        password
      }
    })

    Cookies.set('user_jwt', token)

    commit('SET_USER', user)

    return signIn
  } catch (error) {
    if (error.response && error.response.status === 401) {
      throw new Error('Bad credentials')
    }
    throw new Error(error)
  }
}

export function signOut ({ commit }) {
  try {
    Cookies.delete('user_jwt')

    commit('SET_USER', null)
  } catch (error) {
    throw new Error(error)
  }
}
