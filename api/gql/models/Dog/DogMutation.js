import CreateDogMutation from './mutations/CreateDogMutation'
import DeleteDogMutation from './mutations/DeleteDogMutation'
import UpdateDogMutation from './mutations/UpdateDogMutation'

export default {
  createDog: CreateDogMutation,
  deleteDog: DeleteDogMutation,
  updateDog: UpdateDogMutation
}
