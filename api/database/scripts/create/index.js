import createDatabase from './createDatabase'
import createCollections from './createCollections'

const mainCreateScript = async () => {
  try {
    await createDatabase()
    await createCollections()
  } catch (err) {
    console.log(err)
  }
}

mainCreateScript()
