const isValidDate = maybeDate => !isNaN(maybeDate.getTime())

export default isValidDate
