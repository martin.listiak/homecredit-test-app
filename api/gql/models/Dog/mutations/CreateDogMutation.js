import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt
} from 'graphql'
import { mutationWithClientMutationId } from 'graphql-relay'
import {
  Dog
} from 'database/models'
import DogType from '../DogType'
import { aclUser } from 'validations/userAuthenticated'

const createDogMutation = mutationWithClientMutationId({
  name: 'CreateDogMutation',
  inputFields: {
    name: {
      type: new GraphQLNonNull(GraphQLString)
    },
    age: {
      type: new GraphQLNonNull(GraphQLInt)
    },
    breed: {
      type: new GraphQLNonNull(GraphQLString)
    },
    availableFrom: {
      type: new GraphQLNonNull(GraphQLString)
    },
    availableTo: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  outputFields: {
    dog: {
      type: DogType,
      resolve: (Dog) => Dog
    }
  },
  mutateAndGetPayload: async ({
    name,
    age,
    breed,
    availableFrom,
    availableTo
  }, { req: { user } }) => {
    try {
      aclUser(user)

      const newDog = await Dog.save({
        name,
        age,
        breed,
        availableFrom,
        availableTo,
        owner: user.id
      }, { returnNew: true })

      return newDog.new
    } catch (err) {
      console.log(err)
      throw new Error(err)
    }
  }
})

export default createDogMutation
