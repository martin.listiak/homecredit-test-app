import { GraphQLObjectType } from 'graphql'
import userQuery from './models/User/UserQuery'
import DogQuery from './models/Dog/DogQuery'

const rootFields = Object.assign(
  userQuery,
  DogQuery
)

const rootQuery = new GraphQLObjectType({
  name: 'Query',
  fields: () => rootFields
})

export default rootQuery
