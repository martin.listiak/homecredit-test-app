import SignedInUserQuery from './queries/SignedInUserQuery'

export default {
  SignedInUser: SignedInUserQuery
}
