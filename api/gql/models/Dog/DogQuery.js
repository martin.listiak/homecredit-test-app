import GetAllDogsQuery from './queries/GetAllDogs'

export default {
  GetAllDogs: GetAllDogsQuery
}
