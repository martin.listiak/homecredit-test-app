import { GraphQLEmailType, GraphQLPasswordType } from 'gql/types'
import { GraphQLNonNull, GraphQLString } from 'graphql'
import { mutationWithClientMutationId } from 'graphql-relay'
import User from '../UserType'
import AuthService from 'services/auth'

const signInMutation = mutationWithClientMutationId({
  name: 'SignInMutation',
  inputFields: {
    email: {
      type: new GraphQLNonNull(GraphQLEmailType)
    },
    password: {
      type: new GraphQLNonNull(GraphQLPasswordType)
    }
  },
  outputFields: {
    token: {
      type: GraphQLString,
      resolve: ({ token }) => token
    },
    user: {
      type: User,
      resolve: payload => payload
    }
  },
  mutateAndGetPayload: ({ email, password }, { req }) =>
    AuthService.signIn({ email, password, req })
})

export default signInMutation
