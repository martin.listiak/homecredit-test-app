import {
  GraphQLList
} from 'graphql'
import DogType from '../DogType'
import db from 'database'

export default {
  type: new GraphQLList(DogType),
  description: 'Team of the logged in user',
  resolve: async (parentValues, args, { req }) => {
    const dogs = await db._query(`
      WITH dog, user

      FOR dog IN dogs
        FOR f IN friends
          FILTER u.active == true && f.active == true && u.id == f.userId
    `)

    return dogs
  }
}
