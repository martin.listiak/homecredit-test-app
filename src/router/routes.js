
const routes = [
  {
    path: '',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Home.vue') },
      { path: '/formularova-stranka', component: () => import('pages/FormsPage.vue') },
      { path: '/registracia', component: () => import('pages/SignUp.vue') },
      { path: '/prihlasenie', component: () => import('pages/SignIn.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
