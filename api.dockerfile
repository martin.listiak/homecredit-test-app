FROM node:latest

RUN mkdir /api

WORKDIR /api

COPY . ./

RUN npm install

CMD ["npm", "run", "dev:api"]

EXPOSE 8080
