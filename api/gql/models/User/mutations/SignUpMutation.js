import { GraphQLEmailType, GraphQLPasswordType } from 'gql/types'
import { GraphQLNonNull, GraphQLString } from 'graphql'
import { mutationWithClientMutationId } from 'graphql-relay'
import User from '../UserType'
import AuthService from 'services/auth'

const signUpMutation = mutationWithClientMutationId({
  name: 'SignUpMutation',
  inputFields: {
    email: {
      type: new GraphQLNonNull(GraphQLEmailType)
    },
    username: {
      type: new GraphQLNonNull(GraphQLString)
    },
    password: {
      type: new GraphQLNonNull(GraphQLPasswordType)
    },
    userType: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  outputFields: {
    token: {
      type: GraphQLString,
      resolve: ({ token }) => token
    },
    user: {
      type: User,
      resolve: payload => payload
    }
  },
  mutateAndGetPayload: async ({ email, username, password }, { req }) => {
    try {
      const user = await AuthService.signUp({ email, username, password, req })

      return user
    } catch (err) {
      console.error(err)
      throw new Error(err)
    }
  }
})

export default signUpMutation
